require_relative '../protocols/clear_view_protocol'
require_relative '../protocols/ruby/clear_protocol'
require_relative '../../clearcore/corelibs/ruby/slot_lock_strategy'
require_relative '../../clearcore/corelibs/ruby/ip_utility_code'
require 'socket'
require 'active_record'
require_relative '../clear_admin_portal/config/environment.rb'

include Clear_Protocol
include Clear_View_Protocol

class MessageHandler

  def initialize controller
    TransactionHandler.init
    @controller = controller
    @@tx_number = 0
    @@active_nodes ||= {}
    @@locked_slots ||= {}
    @@requests = {}
    @pending_decoder_request = false
    slots = Slot.all
    slots.each do |slot| 
      slot.locked = false 
      slot.save!
    end

  end

  def handle_master_status_request from, message
    slots_array = []
    @@locked_slots.each_pair do | node, slots | 
      slots_array.push *slots 
    end  
    status_response = MasterStatusResponse.new tx_number: message.tx_number
    status_response.set_active_slots slots_array
    @controller.send_to_scheduler status_response.to_binary, from 
  end 

  def start_engine slot_number
    slot = Slot.find_by_slot_number (slot_number)
    client_ip = slot.node.ip
    start_engine_request = StartEngine.new tx_number: @@tx_number
    start_engine_request.set_slot_number slot_number
    transaction = Transaction.new client_ip, start_engine_request
    TransactionHandler.transactions << transaction
    @controller.send_to_client start_engine_request.to_binary, client_ip
  end 

  def start_test_engine from, message
    slot = Slot.find_by_slot_number message.get_slot_number
    client_ip = slot.node.ip
    start_engine_request = StartEngine.new tx_number: @@tx_number
    start_engine_request.set_slot_number message.get_slot_number
    transaction = Transaction.new from, start_engine_request
    TransactionHandler.transactions << transaction
    @controller.send_to_client start_engine_request.to_binary, client_ip
  end 

  def start_xtraview_engine slot_number
    slot_number = slot_number
    slot = Slot.find_by_slot_number slot_number
    client_ip = slot.node.ip
    start_engine_request = StartXtraview.new tx_number: @@tx_number
    start_engine_request.set_slot_number slot_number
    transaction = Transaction.new client_ip, start_engine_request
    TransactionHandler.transactions << transaction
    @controller.send_to_client start_engine_request.to_binary, client_ip
  end

  def stop_engine slot_number 
    slot = Slot.find_by_slot_number slot_number
    client_ip = slot.node.ip
    stop_engine_request = StopEngine.new tx_number: get_tx_number
    stop_engine_request.set_slot_number message.get_slot_number
    @controller.send_to_client stop_engine_request.to_binary, client_ip
  end

  def handle_stop_engine message
    slot_number = message.get_slot_number
    stop_engine slot_number
  end 

  def handle_start_clear_test from, message
    @pending_decoder_request = true
    slot = Slot.find_by_slot_number message.get_slot_number
    puts "TYPE IS #{@@locked_slots[slot.node_id].class} AND IS #{@@locked_slots[slot.node_id]}"
    if !@@locked_slots[slot.node_id].include? message.get_slot_number.to_i
      new_tx =get_tx_number
      puts "New TX IS #{new_tx}"
      @@requests[new_tx] = message.tx_number
      slot_number = message.get_slot_number 
      start_test_engine from, message
    else 
      puts "ELSE REACHED"
      sleep 1
      data = Nak.new tx_number: message.tx_number
      data.set_error_code 8
      data.set_reason "Cannot start clear test engine, slot is already locked"
      @controller.send_to_client data.to_binary, from
      sleep 1
      @controller.stop_test_server from
    end
  end 

  def handle_stop_clear_test from, message
    @@requests[get_tx_number] = message.tx_number
    slot_number = message.get_slot_number 
    stop_engine slot_number
  end

  def handle_decoder_request message
    @@requests[get_tx_number] = message.tx_number
    decoder_model = message.get_decoder_model
    strategy_locked_slots = []
    @@locked_slots.each_pair do |node, slots|
      slots.each do |slot|
        strategy_locked_slots << slot 
      end
    end
    lock_strategy = EmptyFirst.new @@active_nodes, strategy_locked_slots
    lock_slot = lock_strategy.find_slot decoder_model
    if lock_slot != nil
      puts "Slot found, sending command to start engine"
      puts "Locked slots were #{strategy_locked_slots} and slot #{lock_slot} was locked"
      if decoder_model == "Dualview"
        start_engine (lock_slot + 1) 
        sleep 1
      end  
      start_engine lock_slot 
    else
      puts "No decoder of the requested type is available"
      Log.create(type: "decoder unavailable", content: "a decoder of type #{decoder_model} could not be allocated")
      decoder_response = DecoderRequestResponse.new tx_number: message.tx_number
      decoder_response.set_slot_number 0
      @controller.send_to_client decoder_response.to_binary, check_local_ip
    end
  end

  def handle_dualview_request message
    @@requests[get_tx_number] = message.tx_number
    decoder_model = message.get_decoder_model
    strategy_locked_slots = []
    @@locked_slots.each_pair do |node, slots|
      slots.each do |slot|
        strategy_locked_slots << slot
      end
    end
    lock_strategy = EmptyFirst.new @@active_nodes, strategy_locked_slots
    dualview_primary = lock_strategy.find_slot decoder_model 
    if dualview_primary != nil
      dualview_secondary = dualview_primary + 1
      puts "Dualview found, sending command to start engine"
      start_xtraview_engine dualview_primary
      sleep 1
      start_xtraview_engine dualview_secondary
    else
      puts "Slot found, sending command to start engine"
      Log.create(type: "decoder unavailable", content: "a decoder of type #{decoder_model} could not be allocated")
      decoder_response = DecoderRequestResponse.new tx_number: message.tx_number
      decoder_response.set_slot_number 0
      @controller.send_to_client decoder_response.to_binary, check_local_ip
    end
  end 

  def handle_xtraview_request message
    @@requests[get_tx_number] = message.tx_number
    primary = message.get_primary_decoder_
    secondary = message.get_secondary_decode
    strategy_locked_slots = []
    @@locked_slots.each_pair do |node, slots| 
      slots.each do |slot| 
	strategy_locked_slots << slot
      end
    end 
    lock_strategy = EmptyFirst.new @@active_nodes, strategy_locked_slots
    lock_slots = lock_strategy.find_xtraview primary, secondary
    primary_slot = lock_slots[0]
    puts "primary_slot is #{primary_slot}(#{primary_slot.class})"
    secondary_slot = lock_slots[1]
    puts "secondary_slot is #{secondary_slot}(#{secondary_slot.class})"
    if primary_slot != 0 && secondary_slot != 0
      primary_node_slot = Slot.find_by_id primary_slot
      secondary_node_slot = Slot.find_by_id secondary_slot
      primary_node = Node.find_by_id primary_node_slot.node_id
      secondary_node = Node.find_by_id secondary_node_slot.node_id
      start_xtraview_engine primary_slot
      sleep 1
      start_xtraview_engine secondary_slot
      sleep 1 
      request_status primary_node.id
      sleep 1 
      request_status secondary_node.id
    else
      puts "No decoder of the requested type is available (xtraview)"
      Log.create(type: "xtraview unavailable", content: "a decoder of type #{primary} or #{secondary} could not be allocated")
      xtraview_request_response = XtraviewRequestResponse.new tx_number: message.tx_number
      xtraview_request_response.set_slot_number 0
      @controller.send_to_client xtraview_request_response.to_binary, check_local_ip
      sleep 1
      xtraview_request_response = XtraviewRequestResponse.new tx_number: message.tx_number
      xtraview_request_response.set_slot_number 0
      @controller.send_to_client xtraview_request_response.to_binary, check_local_ip
   end
  end

  def request_status node
    node = Node.find_by_id (node)
    client_ip = node.ip
    status_request = RequestStatus.new tx_number: get_tx_number
    status_request.set_status_type 1
    @controller.send_to_client status_request.to_binary, client_ip
  end	

  def handle_slave_status_response from, message
    if @pending_decoder_request == false
      puts "FROM FOR STATUS RESPONSE IS #{from}"
      node = Node.find_by_ip from
      node_id = node.id
      active_slots = message.get_active_slots
      @@active_nodes[node_id] = active_slots.length
      puts "REPORTED ACTIVE SLOTS ARE #{active_slots}"
      @@locked_slots[node_id] = []
      active_slots.each do |slot|
        @@locked_slots[node_id] << slot
        data_base_slot = Slot.find_by_slot_number slot 
        data_base_slot.locked = true
        data_base_slot.save!
      end 
      node.slots.each do |slot|
        if !@@locked_slots[node_id].include? slot.slot_number
          slot.locked = false
          slot.save!
        end
      end 
    end
  end 

  def handle_ack message
    transaction = TransactionHandler.get_transaction_from_tx_number message.tx_number
    puts 'clearview', "Transaction from: #{transaction.from}, for: #{transaction.message} with tx_number: #{message.tx_number} has completed!"
  end

  def handle_nack message
    transaction = TransactionHandler.get_transaction_from_tx_number message.tx_number
    puts 'clearview', "Transaction from: #{transaction.from}, for: #{transaction.message} with tx_number: #{message.tx_number} has not completed!"
  end

  def handle_decoder_request_response from, message
    @pending_decoder_request = false
    #TODO: the line below this is affecting the mixing of TX numbers, fiddling with it at the moment
    transaction = TransactionHandler.get_transaction_from_tx_number message.tx_number
    slot = Slot.find_by_slot_number message.get_slot_number
    node = Node.find_by_id slot.node_id
    decoder_response = DecoderRequestResponse.new tx_number: @@requests[message.tx_number]
    decoder_response.set_slot_number message.get_slot_number
    decoder_response.set_node_ip message.get_node_ip 
    puts "TRANSACTIONS FROM IS #{transaction.from} and its TX is #{transaction.client_transaction_number}"
    @controller.send_to_client decoder_response.to_binary, transaction.from
    @@locked_slots[node.id] << slot.slot_number
    #@controller.stop_test_server transaction.from
  end

  def handle_xtraview_request_response from, message
    transaction = TransactionHandler.get_transaction_from_tx_number @@requests[message.tx_number]
    xtraview_request_response = XtraviewRequestResponse.new tx_number: @@requests[message.tx_number]
    xtraview_request_response.set_slot_number message.get_slot_number
    @controller.send_to_client xtraview_request_response.to_binary, transaction.from
    slot = Slot.find_by_slot_number message.get_slot_number
    node = Node.find_by_id slot.node_id
    @@locked_slots[node.id] << slot.slot_number
  end

  def get_tx_number
    @@tx_number += 1
    return @@tx_number
  end

  def handle_message from, message
    puts "MESSAGE FROM IS #{from} AND MESSAGE IS #{message} and message is a  #{message.class}"
    if from != nil 
      transaction = Transaction.new from, message
      TransactionHandler.transactions << transaction
    end
    puts "Master-Message Handler-handle_message:#{message} from #{from}"
    case message.cmd_cat
    when ClearProtocolConst.ENGINE_MESSAGES
      puts "Controller-Message Handler-Got ClearEngine Command Type"
      case message.cmd_type
      when ClearProtocolConst.PUSH_BUTTON
        puts "Controller-Message Handler-Got PUSH_BUTTON command"
      when ClearProtocolConst.STOP_ENGINE
        puts "Controller-Message Handler-Got STOP_ENGINE command"
        handle_stop_engine message
      when ClearProtocolConst.START_ENGINE
        puts "Controller-Message Handler-Got start engine command"
        start_engine message.get_slot_number
      end
    when ClearProtocolConst.COMMON_MESSAGES
      log 'clearcommon', "Controller-Message Handler-Got ClearCommon Command Type"
      case message.cmd_type
      when ClearProtocolConst.ACK
        log 'clearcommon', 'Controller-Message Handler-ACK received'
        handle_ack  message
      when ClearProtocolConst.NAK
        log 'clearcommon', 'Controller-Message Handler-NAK received'
        handle_nak  message
      end
    end
    case message.cmd_cat
    when ClearViewProtocolConst.MASTER_MESSAGES
      puts "Controller-Message Handler-Got Master Command Type"
      case message.cmd_type
      when ClearViewProtocolConst.REQUEST_STATUS
        puts "Controller-Message Handler-Status request received"
        handle_master_status_response from 
      when ClearViewProtocolConst.START_CLEAR_TEST
        puts "Controller-Message Handler- Start clear test"
        handle_start_clear_test from, message
      when ClearViewProtocolConst.REQUEST_DECODER
        puts "Controller-Message Handler-Decoder request received"
        if message.get_decoder_model == 'Dualview'
          handle_dualview_request message
        else
          handle_decoder_request message
        end
      when ClearViewProtocolConst.DECODER_REQUEST_RESPONSE
        puts "Controller-Message Handler-Decoder request response received"
        handle_decoder_request_response from, message
      when ClearViewProtocolConst.REQUEST_XTRAVIEW
        puts "Controller-Message Handler-Xtravtew request received"
        handle_xtraview_request message
      when ClearViewProtocolConst.XTRAVIEW_REQUEST_RESPONSE
        puts "Controller-Message Handler-Got Status response command"
        handle_xtraview_request_response from, message
        when ClearViewProtocolConst.MASTER_STATUS_REQUEST
        puts "Controller-Message Handler-Got master status request response command"
        handle_master_status_request from, message
      end
    when ClearViewProtocolConst.SLAVE_MESSAGES
      puts "Controller-Message Handler-Got Slave Command Type"
      case message.cmd_type
      when ClearViewProtocolConst.SLAVE_STATUS_RESPONSE
        puts "Controller-Message Handler-Got Status response command"
        handle_slave_status_response from, message
      else
        puts "Controller-Message Handler-Got Command Type not yet handled in ClearEngine[#{message.cmd_cat}]: #{message.cmd_type} "
      end
    when ClearViewProtocolConst.TEST_MESSAGES
      puts "Controller-Message Handler-Got Test Command Type"
      case message.cmd_type
      when ClearViewProtocolConst.START_CLEAR_TEST
        puts "Controller-Message Handler- Start clear test"
        handle_start_clear_test from, message
      when ClearViewProtocolConst.STOP_CLEAR_TEST
        puts "Controller-Message Handler- Stop clear test"
        handle_stop_clear_test from, message
      end
    end
  end
end

class TransactionHandler

  def self.transactions
    @transactions
  end

  def self.init
    @transactions = []
  end
  
  def self.print_transactions
    p @transactions
  end

  def self.manage_transaction transaction
    @transactions[transaction.server_transaction_number] = transaction
  end

  def self.get_transaction_from_tx_number tx_number
    transaction_copy = nil
    @transactions.each do | transaction | 
      if transaction.client_transaction_number == tx_number
        transaction_copy = transaction
        break
      end
    end
    if transaction_copy 
      @transactions.delete transaction_copy
      return transaction_copy
    end
  end
end

class Transaction

  attr_reader :from, :message, :client_transaction_number, :origin

  def initialize from, message
    @from  = from
    @message = message
    @client_transaction_number = message.tx_number
    @origin = origin
  end

end

