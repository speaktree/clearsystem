require_relative '../protocols/clear_view_protocol'
require_relative '../protocols/ruby/clear_protocol'
require_relative 'controller'
require_relative '../../clearcore/corelibs/ruby/clear_logger'
require_relative 'constants_and_settings'

include Clear_Protocol
include Clear_View_Protocol
include Constants

class ClearMaster
 

  def initialize 
    set_mode SETTINGS.mode
    @controller = Controller.new self
    @controller.start_server
    @controller.start_schedule_server
  end

end

ClearMaster.new

loop do
  sleep(0.01)	
end

