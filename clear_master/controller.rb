require_relative '../../clearcore/corelibs/ruby/tcp_server'
require_relative '../protocols/clear_view_protocol'
require_relative '../protocols/ruby/clear_protocol'
require_relative 'message_handler'
require_relative 'constants_and_settings'

include Clear_Protocol
include Clear_View_Protocol


 class Controller

  def initialize master
    @master = master
    @message_handler = MessageHandler.new self
  end

  def start_server 
    puts "start_server method started"
    begin
      @tcp_server = TCPServerSocket.new 
      @tcp_server.start SETTINGS.master_port, method(:data_method)
      return true
    rescue=>e
      puts "Got Exception: "
      p e
      puts e.backtrace.join("\n")
      return false
    end
  end

  def start_schedule_server
    puts "start_server method started"
    begin
      @schedule_server = TCPServerSocket.new 
      @schedule_server.start (SETTINGS.master_port + 1), method(:data_method)
      return true
    rescue=>e
      puts "Got Exception: "
      p e
      puts e.backtrace.join("\n")
      return false
    end
  end

  def stop_test_server ip
    @tcp_server.close_client ip
  end

  def data_method data, from
    puts "data_method - GOT DATA!!!"
    p data
      message_data = []
      data.each_byte do |b|
        message_data << b
      end
      p message_data
      clear_binary = ClearProtocolBinary.new(:data=>message_data)
      if clear_binary.cmd_cat == ClearProtocolConst.COMMON_MESSAGES || clear_binary.cmd_cat == ClearProtocolConst.ENGINE_MESSAGES
        @message_handler.handle_message from, clear_binary.to_clear_protocol_object
      else
        puts"Identified as clear view message"
        @message_handler.handle_message from, ClearViewProtocolBinary.new(:data=>message_data).to_clear_view_protocol_object
      end
  end

  def send_to_client data, client
    puts "send_to_client method started"
    puts "DATA is #{data}"
    puts "CLIENT IS #{client}"
    @tcp_server.send_data_to data, client
  end

  def send_to_scheduler data, scheduler
    puts "send_to_scheduler method started"
    puts "DATA is #{data}"
    puts "SCHEDULER IS #{scheduler}"
    @schedule_server.send_data_to data, scheduler
  end

end
