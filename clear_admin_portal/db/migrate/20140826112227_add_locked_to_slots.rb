class AddLockedToSlots < ActiveRecord::Migration
  def change
    add_column :slots, :locked, :boolean
  end
end
