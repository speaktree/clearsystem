class CreateSerialServers < ActiveRecord::Migration
  def change
    create_table :serial_servers do |t|
      t.string :ip
      t.integer :node_id
      t.integer :slot_count

      t.timestamps
    end
  end
end
