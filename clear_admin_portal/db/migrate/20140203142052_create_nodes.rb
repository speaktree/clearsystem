class CreateNodes < ActiveRecord::Migration
  def change
    create_table :nodes do |t|
      t.string :name
      t.string :ip
      t.integer :slot_count
      t.integer :ir_netbox_count

      t.timestamps
    end
  end
end
