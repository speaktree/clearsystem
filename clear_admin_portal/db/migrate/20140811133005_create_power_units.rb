class CreatePowerUnits < ActiveRecord::Migration
  def change
    create_table :power_units do |t|
      t.string :ip
      t.integer :node_id
      t.integer :slot_count

      t.timestamps
    end
  end
end
