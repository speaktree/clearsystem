class CreateDecoders < ActiveRecord::Migration
  def change
    create_table :decoders do |t|
      t.string :name
      t.string :model
      t.string :manufacturer
      t.string :serial
      t.integer :smartcard
      t.integer :secure_csn
      t.integer :remote_id
      t.integer :slot_id
      t.string :front_image_url
      t.string :back_image_url

      t.timestamps
    end
  end
end
