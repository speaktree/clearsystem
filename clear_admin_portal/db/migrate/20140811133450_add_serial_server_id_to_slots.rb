class AddSerialServerIdToSlots < ActiveRecord::Migration
  def change
    add_column :slots, :serial_server_id, :integer
  end
end
