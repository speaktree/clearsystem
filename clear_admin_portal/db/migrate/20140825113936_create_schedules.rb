class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.integer :slot_id
      t.string :strategy
      t.datetime :start_time
      t.datetime :end_time

      t.timestamps
    end
  end
end
