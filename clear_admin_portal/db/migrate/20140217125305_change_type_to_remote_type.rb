class ChangeTypeToRemoteType < ActiveRecord::Migration
  def up
  	rename_column :remotes, :type, :remote_type
  end

  def down
  	rename_column :remotes, :remote_type, :type
  end
end
