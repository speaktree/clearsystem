class AddFrequencyTimerCountToButtons < ActiveRecord::Migration
  def change
    add_column :buttons, :frequency_timer_count, :string
  end
end
