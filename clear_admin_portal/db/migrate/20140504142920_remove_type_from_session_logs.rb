class RemoveTypeFromSessionLogs < ActiveRecord::Migration
  def up
    remove_column :session_logs, :type
  end

  def down
    add_column :session_logs, :type, :string
  end
end
