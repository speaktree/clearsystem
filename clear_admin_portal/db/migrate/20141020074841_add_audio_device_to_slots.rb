class AddAudioDeviceToSlots < ActiveRecord::Migration
  def change
    add_column :slots, :audio_device, :integer
  end
end
