class AddClientHostToSessionLogs < ActiveRecord::Migration
  def change
    add_column :session_logs, :client_host, :string
  end
end
