class AddBuildToSchedules < ActiveRecord::Migration
  def change
    add_column :schedules, :build, :string
  end
end
