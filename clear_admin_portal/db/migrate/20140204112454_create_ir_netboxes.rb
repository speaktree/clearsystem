class CreateIrNetboxes < ActiveRecord::Migration
  def change
    create_table :ir_netboxes do |t|
      t.string :ip
      t.integer :port
      t.integer :node_id
      t.integer :slot_count

      t.timestamps
    end
  end
end
