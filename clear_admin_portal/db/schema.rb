# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150113144352) do

  create_table "buttons", :force => true do |t|
    t.binary   "alphabet",              :limit => 255
    t.binary   "ir_representation"
    t.string   "name"
    t.integer  "button_command"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.string   "frequency_timer_count"
    t.integer  "alphabet_length"
  end

  create_table "decoders", :force => true do |t|
    t.string   "name"
    t.string   "model"
    t.string   "manufacturer"
    t.string   "serial"
    t.integer  "smartcard"
    t.integer  "secure_csn"
    t.integer  "remote_id"
    t.integer  "slot_id"
    t.string   "front_image_url"
    t.string   "back_image_url"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "gui_buttons", :force => true do |t|
    t.string   "image_url"
    t.integer  "x"
    t.integer  "y"
    t.integer  "width"
    t.integer  "height"
    t.integer  "button_id"
    t.integer  "remote_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "ir_netboxes", :force => true do |t|
    t.string   "ip"
    t.integer  "port"
    t.integer  "node_id"
    t.integer  "slot_count"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "logs", :force => true do |t|
    t.string   "type"
    t.string   "content"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "node_types", :force => true do |t|
    t.string   "node_type"
    t.integer  "node_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "nodes", :force => true do |t|
    t.string   "name"
    t.string   "ip"
    t.integer  "slot_count"
    t.integer  "ir_netbox_count"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "external_ip"
  end

  create_table "power_units", :force => true do |t|
    t.string   "ip"
    t.integer  "node_id"
    t.integer  "slot_count"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "remotes", :force => true do |t|
    t.string   "name"
    t.string   "model"
    t.string   "image_url"
    t.string   "remote_type"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "schedules", :force => true do |t|
    t.integer  "slot_id"
    t.string   "strategy"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "repo"
    t.string   "branch"
    t.string   "xml"
    t.string   "product"
    t.integer  "repeats"
    t.date     "repeat_end"
    t.string   "build"
  end

  create_table "serial_servers", :force => true do |t|
    t.string   "ip"
    t.integer  "node_id"
    t.integer  "slot_count"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "session_logs", :force => true do |t|
    t.string   "start"
    t.string   "session_end"
    t.integer  "slot"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "query_type"
    t.string   "client_ip"
    t.string   "client_host"
    t.integer  "xtraview_slot"
  end

  create_table "slots", :force => true do |t|
    t.integer  "slot_number"
    t.integer  "node_id"
    t.integer  "ir_netbox_id"
    t.integer  "ir_slot"
    t.string   "input_codec"
    t.string   "size"
    t.string   "input_format"
    t.string   "video_device"
    t.string   "output_codec"
    t.string   "output_format"
    t.integer  "bitrate"
    t.integer  "framerate"
    t.string   "output_url"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "serial_server_id"
    t.integer  "serial_server_slot"
    t.integer  "power_unit_id"
    t.integer  "power_unit_slot"
    t.boolean  "locked"
    t.integer  "audio_device"
  end

  create_table "xtraview_slots", :force => true do |t|
    t.integer  "primary_slot_id"
    t.integer  "secondary_slot_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

end
