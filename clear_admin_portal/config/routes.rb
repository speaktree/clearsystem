ClearAdminPortal::Application.routes.draw do
  get "session_log/sessionlogs"
  get "session_log/session_log_fields"
  post "session_log/update_log_field_count"
  get "node/nodes"
  post 'node/update_node'
  get "node/new_node"
  post "node/delete_node"
  post "node/create_node"
  get "slot/slots"
  get "slot/slot_video"
  get "slot/new_slot"
  post "slot/create_slot"
  post 'slot/update_slot'
  post 'slot/update_slot_video'
  post "slot/delete_slot"
  get "xtraview/xtraviews"
  post 'xtraview/update_xtraview'
  post 'xtraview/create_xtraview'
  post 'xtraview/delete_xtraview'
  get "xtraview/new_xtraview"
  get "decoder/decoders"
  post 'decoder/update_decoder'
  post 'decoder/delete_decoder'
  post "decoder/create_decoder"
  get "decoder/new_decoder"
  get "ir_netbox/ir_netboxes"
  post "ir_netbox/update_ir_netbox"
  get "ir_netbox/new_ir_netbox"
  post "ir_netbox/create_ir_netbox"
  post "ir_netbox/delete_ir_netbox"
  get "remote/remotes"
  post "remote/update_remote"
  post "remote/delete_remote"
  post "remote/create_remote"
  get "remote/new_remote"
  get "power_unit/power_units"
  post 'power_unit/update_power_unit'
  post 'power_unit/delete_power_unit'
  post 'power_unit/create_power_unit'
  get "power_unit/new_power_unit"
  post 'admin/update_view_slot'
  get "serial_server/serial_servers"
  post 'serial_server/update_serial_server'
  post 'serial_server/delete_serial_server'
  post 'serial_server/create_serial_server'
  get "serial_server/new_serial_server"
  get "admin/index"
  get "gui_button/gui_button_fields"
  post "gui_button/update_active_remote"
  get "gui_button/gui_buttons"
  post 'gui_button/update_gui_button'
  get "button/buttons"
  post "button/update_button"
  get "admin/active_slots"
  get "admin/database_index"
  get "admin/active_slots_sidebar"
  get "admin/vlc"
  get "admin/lock_slot"
  post "admin/unlock_slot"
  root :to => redirect('/admin/index')

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
