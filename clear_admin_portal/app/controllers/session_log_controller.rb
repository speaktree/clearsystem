
class SessionLogController < ApplicationController


  def sessionlogs
    render "sessionlogs"
  end

  def session_log_fields
    @log_field_count = session[:log_field_count]
  end

  def update_log_field_count
    session[:log_field_count] = params[:log_field_count].to_i
    render :nothing => true
  end

end
