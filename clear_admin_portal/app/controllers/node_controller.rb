class NodeController < ApplicationController

  def nodes 
    @nodes = Node.all
    @node_types = NodeType.all
  end 

  def update_node
    node_id = params[:node_id]
    node = Node.find_by_id node_id
    node_type = NodeType.find_by_node_id node_id
    node.name = params[:name]
    node.ip = params[:ip]
    node.external_ip = params[:external_ip]
    node.slot_count = params[:slot_count]
    node.ir_netbox_count = params[:ir_netbox_count]
    node_type.node_type = params[:node_type]
    node_type.node_id = params[:node_id]
    node.save!
    node_type.save!
    render :nothing => true
  end 

  def create_node
    node = Node.new
    node.name = params[:name]
    node.ip = params[:ip]
    node.slot_count = params[:slot_count]
    node.ir_netbox_count = params[:ir_netbox_count]
    node.external_ip= params[:external_ip]
    node.save!

    node_type = NodeType.new
    node_type.node_id = node.id
    node_type.node_type = params[:node_type]
    node_type.save!
    render :nothing => true
  end

  def delete_node 
    node_id = params[:node_id]
    node = Node.find_by_id node_id 
    node_type = NodeType.find_by_node_id node_id
    node.delete
    node_type.delete
    render :nothing => true
  end

end 
