class PowerUnitController < ApplicationController

  def power_units 
    @power_units = PowerUnit.all
  end 

  def update_power_unit
    power_unit_id = params[:power_unit_id]
    power_unit = PowerUnit.find_by_id power_unit_id
    power_unit.ip = params[:ip]
    power_unit.node_id = params[:node_id]
    power_unit.slot_count = params[:slot_count]
    power_unit.save!
    render :nothing => true
  end

  def create_power_unit
    power_unit = PowerUnit.new
    power_unit.ip = params[:ip]
    power_unit.node_id = params[:node_id]
    power_unit.slot_count = params[:slot_count]
    power_unit.save!
    render :nothing => true
  end
  
  def delete_power_unit
    power_unit_id = params[:power_unit_id]
    power_unit = PowerUnit.find_by_id power_unit_id
    power_unit.delete
    render :nothing => true
  end

end 
