class SerialServerController < ApplicationController

  def serial_servers
    @serial_servers = SerialServer.all
  end
  
  def update_serial_server
    serial_server_id = params[:serial_server_id]
    serial_server = SerialServer.find_by_id serial_server_id
    serial_server.ip = params[:ip]
    serial_server.node_id = params[:node_id]
    serial_server.slot_count = params[:slot_count]
    serial_server.save!
    render :nothing => true
  end

  def create_serial_server
    serial_server = SerialServer.new
    serial_server.ip = params[:ip]
    serial_server.node_id = params[:node_id]
    serial_server.slot_count = params[:slot_count]
    serial_server.save!
    render :nothing => true
  end
  
  def delete_serial_server
    serial_server_id = params[:serial_server_id]
    serial_server = SerialServer.find_by_id serial_server_id
    serial_server.delete
    render :nothing => true
  end

end 
