class IrNetboxController < ApplicationController

  def ir_netboxes
    @ir_netboxes = IrNetbox.all
  end

  def update_ir_netbox
    ir_netbox_id = params[:ir_netbox_id]
    ir_netbox = IrNetbox.find_by_id ir_netbox_id
    ir_netbox.ip = params[:ip]
    ir_netbox.port = params[:port]
    ir_netbox.node_id = params[:node_id]
    ir_netbox.slot_count = params[:slot_count]
    ir_netbox.save!
    render :nothing => true
  end

  def delete_ir_netbox
    ir_netbox_id = params[:ir_netbox_id]
    ir_netbox = IrNetbox.find_by_id ir_netbox_id
    ir_netbox.delete
    render :nothing => true
  end 

  def create_ir_netbox
    ir_netbox = IrNetbox.new
    ir_netbox.ip = params[:ip]
    ir_netbox.node_id = params[:node_id]
    ir_netbox.port = params[:port]
    ir_netbox.slot_count = params[:slot_count]
    ir_netbox.save!
    render :nothing => true
  end 

end 
