class Button < ActiveRecord::Base
  attr_accessible :alphabet, :ir_representation, :name, :button_command, :frequency_timer_count

  has_one :gui_button
  has_one :remote, through: :gui_button

  validates_presence_of :ir_representation, :name, :button_command
  validates :name, uniqueness: true
  #validates :alphabet, length: { is: 515} 

  def alphabet_array
  	return alphabet.scan(/\d+/).map(&:to_i)
  end

	def ir_representation_array
  	return ir_representation.scan(/\d+/).map(&:to_i)
  end

end
