class Node < ActiveRecord::Base
	
  attr_accessible :ip, :ir_netbox_count, :name, :slot_count

	has_many :ir_netboxes
	has_many :slots

  validates :ip, :ir_netbox_count, :name, :slot_count, presence: true
  validates :ir_netbox_count, :slot_count, numericality: { only_integer: true }
	validates :ip, format: { with: /^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/ , message: "must be an IP address"}
  validates :ip, :name, uniqueness: true
  
end
