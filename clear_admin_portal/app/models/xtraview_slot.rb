class XtraviewSlot < ActiveRecord::Base
  attr_accessible :primary_slot_id, :secondary_slot_id

  belongs_to :slot

	validates :primary_slot_id, :secondary_slot_id, presence:true
 	validates :primary_slot_id, :secondary_slot_id, numericality: { only_integer: true }
 	validates :primary_slot_id, :secondary_slot_id, uniqueness: true

end
