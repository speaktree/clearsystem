class NodeType < ActiveRecord::Base
  attr_accessible :node_id, :node_type

  belongs_to :node

  validates :node_id, :node_type, presence: true
  validates :node_id, uniqueness: true
  validates :node_id, numericality: { only_integer: true }
  validates :node_type , inclusion: { in: %w(master slave), message: 'Value must be either "master" or "slave"'  } 

end
