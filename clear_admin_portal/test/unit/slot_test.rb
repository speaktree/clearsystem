require 'test_helper'

class SlotTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "slot has decoder" do
    slot = Slot.find_by_slot_number(1)
    assert_not_nil slot.decoder
    assert_equal 'Testing Decoder 1', slot.decoder.name
  end

end
