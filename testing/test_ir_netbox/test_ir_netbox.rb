$: << '.'

require 'test/unit'

include Test::Unit::Assertions

require_relative '../../../clearcore/corelibs/ruby/udp_server'
require_relative '../../../clearcore/corelibs/ruby/tcp_client'
require_relative '../../protocols/ruby/ir_protocol'

include IR_Protocol

@received_count = 0
@sent = false
@received = false

def udp_message_handler data
	puts "DATA RECEIVED: |#{data}|"
	byte_data = []
	data.each_char {|c| byte_data << c.ord}
	irp_binary = nil
    if @received_count == 0
      @received_count += 1
      discovery_response = [0x00, 0x1E, 0xFE] + byte_data
      irp_binary = IRProtocolBinary.new :data=>discovery_response
      discovery_object = irp_binary.to_ir_protocol_object
      puts "Firmware Image Id: #{discovery_object.body[8].chr}#{discovery_object.body[9].chr}"
      mac_hex = ""
      discovery_object.get_mac_address.each_index do |i|
        if i == 0
          val = discovery_object.get_mac_address[i] | 0b01000000 # Set bit 7 high
          mac_hex << val.to_i.to_s(16).upcase << ":"
        else
          mac_hex << discovery_object.get_mac_address[i].to_s(16).upcase << ":"
          if i == 2
            mac_hex << "FF:FE:"
          end
        end 	
      end
      puts "MAC Address:[#{mac_hex[0...-1]}]"
    else
      irp_binary = IRProtocolBinary.new :data=>byte_data
    end 
    @received = true
end

def test_udp_broadcast_message

	# Create a UDP Server
	udp_server = UDPServerSocket.new
	udp_server.start(55555, method(:udp_message_handler))

	# Broadcast the message to find the irNetBox
	discovery = Discovery.new
	discovery.set_data_block 0xF6
	data_to_send = discovery.to_binary
	puts "Broadcasting data: #{data_to_send[3..-1]}"
	@received = false
	udp_server.broadcast_data(data_to_send[3..-1] ,30718)
    @sent = true
	1.upto(2) do 
	    puts 'Sleeping 5 seconds...'  
		sleep 5
	end

	udp_server.stop

end

IRNETBOX_IP = '10.17.183.147'
IRNETBOX_PORT = 10001

@responded = false
@byte_data = []
def handle_message data 
  data.each_char {|c| @byte_data << c.ord}
  puts "Received data: |#{@byte_data}|"
  if @byte_data.length < 3
    return
  else
    byte_data = []
    if @byte_data.length > 4
      byte_data = @byte_data[0..3]
      p byte_data
      @byte_data = @byte_data[4..-1]
      p @byte_data
    else
      byte_data = @byte_data
    end
    @responded = true
    ir_binary = IRProtocolBinary.new :data => byte_data
    ir_object = ir_binary.to_ir_protocol_object
    if ir_object.class == Error
      puts "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!GOT ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
      puts "Error Number:#{ir_object.get_code}"
      puts "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!GOT ERROR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    else
      puts "ALL IS GOOD-Received:#{ir_object.class} :)!"
      #TODO: Do something with the data
    end
    @byte_data = []
  end
end

@connected = false
@count = 0

def handle_notification host, state, message
  puts "Received notification: #{host}-#{state}-#{message}"
  if state && message == "Connected!"
  	@connected = true
  end
end

# All outgoing messages needs a '#' infront. It is just because these guys that created this protocol are retards!!
def common_send data
  @responded = false
  to_send = [35]
  return to_send + data
end

def wait_response
  @count = 0
  while !@responded && @count < 50
  	puts 'Waiting for response...'
    sleep 1
    @count += 1
  end
end

def test_tcp_connect
  @tcp_client = TCPSocketClientSocket.new
  @tcp_client.connect IRNETBOX_IP, IRNETBOX_PORT, method(:handle_message), method(:handle_notification)
  # Just sleep some time to wait for the async connect to finish
  while !@connected && @count < 5
    sleep 1
    @count += 1
  end
  assert_equal(true, @connected)
  # The sequence of instructions to do on connection - The irAPI will do this when it comes up
  # Power on CPLD
  power_on = PowerOn.new
  @tcp_client.send common_send power_on.to_binary
  wait_response
  assert_equal(true, @responded)
  # Reset the CPLD
  cpld_instruction = CPLDInstruction.new 
  cpld_instruction.set_code 0x00
  @tcp_client.send common_send cpld_instruction.to_binary
  wait_response
  assert_equal(true, @responded)
  # Enable Ir Slots 16 - Don't use this one it is confusing the other one is b
  bit = 0
  0.upto(16) do
    saoubmb =  SetAllOutputsUsingBitMaskBlock.new 
	val = 0
	val = val | 2 ** bit
	val = val | 2 ** bit + 1
	saoubmb.set_port_states val
	@tcp_client.send common_send saoubmb.to_binary
	wait_response
	assert_equal(true, @responded)
	bit += 2
  end
  # Enable Ir Slots 1-8
  saoubms =  SetAllOutputsUsingBitMaskSeperated.new 
  saoubms.set_states_port1_to4 0b11001100  #4,3,2,1
  saoubms.set_states_port5_to8 0b11001100  #8,7,6,5
  @tcp_client.send common_send saoubms.to_binary
  wait_response
  assert_equal(true, @responded)
  # Set enabled LEDs
  cpld_instruction = CPLDInstruction.new 
  cpld_instruction.set_code 0x17
  @tcp_client.send common_send cpld_instruction.to_binary
  wait_response
  assert_equal(true, @responded)
end

def get_binary_string_for command = nil
  # Channel + Data
  channel_plus_string = "000132B7FF600000000400000088013D851F5A03BD0BCD000000000000000000000000000000000000000000000000000102020203020302020203020302030203020302020202020302020203020202020202020302020202020302020203020302030202020302030202020202020202027F000102020203020302020203020302030203020302020202020302020203020202020202020302020202020302020203020302030202020302030202020202020202027F"
  channel_plus_alphabet = "3D851F5A03BD0BCD000000000000000000000000000000000000000000000000" # Size in DB 32 bytes
  # This will be in the ir representations table two enries to simulate the channel up button which are in the buttons table
  # Each section will be an 68 bytes of binary blob data -> To send it you just put all the sections together :)
  # 000102020203020302020203020302030203020302020202020302020203020202020202020302020202020302020203020302030202020302030202020202020202027F
  # 000102020203020302020203020302030203020302020202020302020203020202020202020302020202020302020203020302030202020302030202020202020202027F

  channel_plus_ir_representation = "000102020203020302020203020302030203020302020202020302020203020202020202020302020202020302020203020302030202020302030202020202020202027F000102020203020302020203020302030203020302020202020302020203020202020202020302020202020302020203020302030202020302030202020202020202027F"
  channel_plus_string = channel_plus_string.scan(/../)
  channel_plus_data = []
  channel_plus_string.each do |number|
    channel_plus_data << number.to_i(16)
  end 
  p channel_plus_data
  return channel_plus_data[15..-1] # Just cut of the header in the current data
end

def test_ir_command
  puts "[][][][][][][][][][][][][][][][][][][][][][][][][][][][][][][]"
  puts "Test IR Command"
  # Assign memory for the ir command
  allocate_memory = AllocateMemoryIRSignal.new 
  @tcp_client.send common_send allocate_memory.to_binary
  wait_response
  assert_equal(true, @responded)
  channel_plus_ir_data = get_binary_string_for
  download_ir_signal_data = DownloadIRSignalData.new
  download_ir_signal_data.set_intra_signal_pause 100
  download_ir_signal_data.set_frequency_timer_count 65376
  download_ir_signal_data.set_number_of_periods 0
  download_ir_signal_data.set_maximum_number_lengths 0
  download_ir_signal_data.set_actual_number_lengths 4
  download_ir_signal_data.set_maximum_signal_data_size 0
  download_ir_signal_data.set_actual_signal_data_size channel_plus_ir_data.length - 32
  download_ir_signal_data.set_number_of_signal_repeats 1 
  download_ir_signal_data.set_length_and_signal_data channel_plus_ir_data
  0.upto(100) do 
    @tcp_client.send common_send download_ir_signal_data.to_binary
    wait_response
    assert_equal(true, @responded)
    
    output_ir_signal = OutputIRSignalData.new
  
    @tcp_client.send common_send output_ir_signal.to_binary
    wait_response
    assert_equal(true, @responded)
  end
  puts "/'/'/'/'/'/'/'/'/'/'/'/'/'/'/'/'/'/'/'/'/'/'/'/'/'/'/'/'/'/'/"
end

def test_tcp_diconnect
  # The sequence of instructions to do on disconnection - The irAPI will do this when it goes down or when it needs to (hopefully never)
  # Reset the CPLD
  cpld_instruction = CPLDInstruction.new 
  cpld_instruction.set_code 0x00
  @tcp_client.send common_send cpld_instruction.to_binary
  wait_response
  assert_equal(true, @responded)
  # Power on CPLD
  power_on = PowerOff.new
  @tcp_client.send common_send power_on.to_binary
  wait_response
  assert_equal(true, @responded)
  @tcp_client.disconnect
end

get_binary_string_for

puts "################################################################"
#Tests
puts "Test UDP Broadcast Interaction"
test_udp_broadcast_message
assert_equal(true, @sent)
assert_equal(true, @received)
puts "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-"
puts "################################################################"
#Tests
puts "Test TCP Interaction"
test_tcp_connect
test_ir_command
test_tcp_diconnect
puts "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-"

