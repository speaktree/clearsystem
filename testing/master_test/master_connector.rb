require 'socket'
require 'sqlite3'
require 'active_record'
require_relative '../../clearcommon/clear_admin_portal/config/environment.rb'
require_relative '../../clearcommon/corelibs/ruby/tcp_client'
require_relative '../../protocols/clear_view_protocol'
require_relative '../../clearcommon/protocols/ruby/clear_protocol'
require_relative 'constants_and_settings'

include Clear_View_Protocol
include Clear_Protocol
include Constants 

class MasterConnector

  @@tx_number = 0
  
  def start_client 
    begin
      master_node = NodeType.find_by_node_type('master')
      @master_ip = master_node.node.ip
      @master_port = SETTINGS.master_port
      @tcp_client = TCPSocketClientSocket.new 
      @tcp_client.connect @master_ip , @master_port , method(:data_method), method(:notification_method)
      return true
    rescue=>e
      puts "Got Exception: "
      p e
      puts e.backtrace.join("\n")
      return false
    end
  end

  def status_request
    status_request = RequestStatus.new tx_number: get_tx_number
    status_request.set_status_type 0
    @tcp_client.send status_request.to_binary
  end

  def start_engine slot_number
    start_engine = StartEngine.new tx_number: get_tx_number
    start_engine.set_slot_number slot_number
    @tcp_client.send start_engine.to_binary
  end
     
  def stop_engine slot_number
    stop_engine = StopEngine.new tx_number: get_tx_number
    stop_engine.set_slot_number slot_number
    @tcp_client.send stop_engine.to_binary
  end

  def get_tx_number
    @@tx_number += 1
  end

  def data_method data, from
    puts "data_method - GOT DATA!!!"
    p data
    if data.include? ']'
      split = data.strip.gsub('[', '').split(']')
      split.each do |message|
        split_message = message.split(',')
        message_data = []
        split_message.each do |val|
          message_data << val.to_i
        end
        @message_handler.handle_message from, ClearProtocolBinary.new(:data=>message_data).to_clear_protocol_object
      end
    else
      message_data = []
      data.each_byte do |b|
        message_data << b
      end
      @message_handler.handle_message from, ClearProtocolBinary.new(:data=>message_data).to_clear_protocol_object
    end
  end

  def notification_method source, state, message
    puts 'Controller notification_method called!'
    p source
    p state
    p message
  end
end
  
if __FILE__ == $0

    
master_connector = MasterConnector.new
master_connector.start_client
sleep(3)
#master_connector.start_engine 1
#sleep(30)
master_connector.status_request 

end
