# J Viljoen 2014-03-02
#
# The purpose of this module is to keep track of which
# commands were sent and which ones were responded to 
# and in what way.
# i.e. we are going to give a 
#       'do_infrared 2' command and should receive an 
#       'ACK' message back
# The messages are tied together with the tx_number
#


$: << '.'
require_relative '../../corelibs/ruby/clear_logger'

module Transaction_Module

  def add_transaction tx_number, message
    if @transactions == nil
      @transactions = []
    end
    transaction_item = TransactionItem. new(tx_number, message)
    @transactions.push transaction_item
  end

  def get_transaction_for_tx_number tx_number
    @transactions.each { |t| 
      if t.get_number == tx_number
        return t
      end
    }
  end

  # if a transaction is ack'd we'll just log the item as done & ack'd
  # then we'll remove it from the array
  def ack_transaction tx_number
    found = false
    @transactions.each { |t|
      if t.get_number == tx_number
        # remove item from array
        @transactions.delete t
        found = true
        break   # get out of loop
      end
    }
    if !found
      # log error for no ack to message match
      log :warn, "tx number: #{tx_number} Couldn't match ACK to message"
      return false
    else
      return true
    end
  end
  
  # if a transaction is Nack'd we'll log the item as nacked
  # then we'll remove it from the array
  def nack_transaction tx_number
    found = false
    @transactions.each { |t|
      if t.get_number == tx_number
        # remove item from array
        @transactions.delete t
        found = true
      end
    }
    if !found
      # log error for no nack to message match
      log :warn, "tx number: #{tx_number} Couldn't match NACK to message"
      return false
    else
      return true
    end
  end

  def get_latest_transaction
    if @transactions.length > 0
      return @transactions[@transactions.length - 1]
    else
      return nil
    end
  end
end

class TransactionItem
  @tx_number = -1
  @message = ""
  @result = ""

  def initialize tx_number, message
    @tx_number = tx_number
    @message = message
    @result = ""
  end

  def get_number
    @tx_number
  end

  def get_message
    @message
  end

  def set_result result
    @result = result
  end

  def get_result
    @result
  end
end
