$: << '.'

=begin

Test harnass for Clearview system
2014 February

Testing Harnass -> System behavioural level testing

- Design console based testing app to execute input
	e.g. 
		1. Start Engine
		2. Stop Engine
		3. Start Video
		4. Stop Video
- It must be function driven or modular enough so that an ASCII command interpreter
	can be written to call the same functionality as doing it manually

- While doing this think about handling and what feedback the system must give so
	that the harnass and the system reacts the same

=end

require 'rubygems'
require_relative 'test_harness_modules'

include Test_Module


# Main loop
Test_Module.check_and_load_test_config_file

scripted_test = false
# Check if argument file passed to app
# 2013-03-21 expand this functionality
# the program should discern between a single test file or a folder. If a single file
# just run that, but when a folder open each file and then run it in that folder.
if ARGV.count > 0
  scripted_test = true
  # discern if the input argument was a folder name or a filename
  if Dir.exists?(ARGV.first)
    list_of_files = Dir.entries(ARGV.first)
    list_of_files.each do |file|
      if file.end_with? ".txt"
        puts "Run sripted test-> #{file}"
        p File.join(ARGV.first,file)
        Test_Module.read_script_file(File.join(ARGV.first,file))
      end
    end
    puts "Finished processing #{ARGV.first}"
  else
    filename = ARGV.first
    if File.exists?(filename)
      puts "Scripted test to execute file #{ARGV[1]}"
      #log :info, msg
      Test_Module.read_script_file(filename)
    end
  end
end

continue = !scripted_test #Do not continue if the scripted tests are running
while continue

  puts "\n\n\n\tClearView Exercise Test App " + Time.new.inspect 
  puts "\t0. Configuration"
  puts "\t1. Engine Commands"
  puts "\t2. Start Video"
  puts "\t3. Stop Video"
  puts "\t4. Push IR button"
  puts "\t5. Test TCP_Client"
  puts "\t7. Test junit modules"
  puts "\t8. Test transactions"
  puts "\t9. to Quit"
  print "\n\tEnter selection: "
  selection = gets.chomp()

  case selection
  when "0"
    in_configloop = true;
    while in_configloop
      puts "\n\n\t1. Remote ip: #{Test_Module.get_ip_address}:#{Test_Module.get_port_number}"
      puts "\t2. Selected slot: #{Test_Module.get_slot_nr}"
      print "\t3. "
      if Test_Module.get_tcp_or_stdout == "tcp"
        puts "Tcp output mode"
      end
      if Test_Module.get_tcp_or_stdout == "stdout"
        puts "Output to screen"
      end

      print "\t9. to exit this menu\n\t"
      config_loop_selection = gets.chomp()

      case config_loop_selection
      when "1"	#Change IP and port
        print "\tNew IP (xxx.xxx.xxx.xxx):"
        Test_Module.set_ip_address gets.chomp()
        print "\t\tnew Port (xxxx):"
        Test_Module.set_port_number gets.chomp()
      when "2"
        print "\t\tNew slot nr: " 
        a = {:slot_number => STDIN.gets.chomp()}
        if Test_Module.validate(a)
          Test_Module.set_slot_nr a[:slot_number]
        else
          puts "\n\t\tSlot selection #{a[:slot_number]} not valid"
        end
      when "3"
        case Test_Module.get_tcp_or_stdout
        when "tcp"
          Test_Module.set_tcp_or_stdout "stdout"
        when "stdout"
          Test_Module.set_tcp_or_stdout "tcp"
        end
      when "9"
        in_configloop = false
      end
    end
  when "1"
    engine_commands_loop = true
    while engine_commands_loop
      print "\n\n\n\t\tEngine commands"
      if Test_Module.get_engine_running
        print "\t*** Engine running currently, PID\##{Test_Module.get_pid_of_interest} ***"
      end
      puts "\n\t\t0. Configure engine"
      puts "\t\t1. Start engine locally"
      puts "\t\t2. Stop engine locally"
      puts "\t\t9. Back"
      engine_choice = STDIN.gets.chomp()
      case engine_choice
      when "0"
        loop_engine_config = true
        while loop_engine_config
          puts "\n\t\tConfiguration selected"
          puts "\t\t1. Current selected engine slot nr: #{Test_Module.get_engine_slot_nr}"
          puts "\t\t9. Back"
          engine_config_choice = STDIN.gets.chomp()
          case engine_config_choice
          when "1"
            print "\t\tNew engine slot nr: "
            a = {:slot_number => STDIN.gets.chomp()}
            if Test_Module.validate(a)
              Test_Module.set_engine_slot_nr a[:slot_number].to_i
            end
          when "9"
            loop_engine_config = false
          end
        end
      when "1"
        puts "\n\tStart engine selected"
        Test_Module.start_engine
        puts "Pid value received: #{Test_Module.get_pid_of_interest}"
      when "2"
        puts "\n\tStop engine selected"
        msg = Test_Module.stop_engine
        if msg != nil
          puts msg
        end
      when "9"
        engine_commands_loop = false
      end
    end
  when "2"
    puts "\n\tStart video selected"
  when "3"
    puts "\n\tStop video selected"
  when "4"
    puts	"\n\tIR Pushbutton selected"
    irloop = true
    #mode = 't'
    while irloop
      puts "\t\t1. Standby\t\t\t24. Help"
      puts "\t\t2. Volume up\t\t\t25. PlayList"
      puts "\t\t3. Volume down\t\t\t26. Search"
      puts "\t\t4. Channel up\t\t\t27. Arc"
      puts "\t\t5. Channel down\t\t\t28. Exit"
      puts "\t\t7. Mute\t\t\t\t29. Tv"
      puts "\t\t8. Backup\t\t\t30. Profile"
      puts "\t\t9. TV Guide\t\t\t31. Left"
      puts "\t\t10. Info\t\t\t32. Right"
      puts "\t\t11. Menu\t\t\t33. Up"
      puts "\t\t12. GO TV\t\t\t34. Down"
      puts "\t\t13. Rewind\t\t\t35. 0"
      puts "\t\t14. Play\t\t\t36. 1"
      puts "\t\t15. Stop\t\t\t37. 2"
      puts "\t\t16. Pause\t\t\t38. 3"
      puts "\t\t17. Record\t\t\t39. 4"
      puts "\t\t18. Fast Forward\t\t40. 5"
      puts "\t\t19. Ok\t\t\t\t41. 6"
      puts "\t\t20. On Demand\t\t\t42. 7"
      puts "\t\t21. Alt\t\t\t\t43. 8"
      puts "\t\t22. BoxOffice\t\t\t44. 9"
      puts "\t\t23. Option"
      puts "\t\tX. Exit to previous menu"
      print "\t\tSelection: "
      irselect = gets.chomp()

      case irselect
      when "1".."44"
        return_msg = Test_Module.do_infrared irselect.to_i
        #if return_msg != '' || return_msg != nil
        #  puts return_msg
        #end
      when "x", "X"
        irloop = false
      end #case irselect

    end #irloop

  when "5"
    # Test TCP Client
    puts "\n\t\tCurrent IP address: #{Test_Module.get_ip_address}:#{Test_Module.get_port_number}"
    print "\t\tStarting test\n\t\t"
    info_from_test = Test_Module.test_tcp_comms
    puts info_from_test
  when "7"
    Test_Module.test_junit_models
  when "8"
    Test_Module.test_transactions
  when "9"
    Test_Module.shutdown
    puts "\n\tGoodbye...\n\n"
    continue = false
  else
    puts "\n\tNo valid value selected"
  end #case
end #while


