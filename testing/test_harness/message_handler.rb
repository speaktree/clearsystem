require_relative '../../corelibs/ruby/tcp_client'
require_relative '../../protocols/ruby/clear_protocol'
require_relative '../../corelibs/ruby/clear_logger'
require_relative 'transaction_module'

include Clear_Protocol
include Transaction_Module

# Handles all the various messages that it should or could and creates repsonses that gets passed back to through te controller.
# This message_handler customised to suit test_harness since it is a little too integrated with the
# clear_engine controller to suite the test_harness' needs
# Jaques 2014-02-25

module MessageHandler
  def handle_message message
    # puts "************************test msg handler************************"
    # puts "Test Harness-Message Handler-handle_message:#{message}"
    case message.cmd_cat
    when ClearProtocolConst.COMMON_MESSAGES
      case message.cmd_type
      when ClearProtocolConst.ACK
        # puts "Test Harness-Message Handler-ACK received"
        #Transaction_Module.ack_transaction message.tx_number
        ti = TransactionItem.new message.tx_number, message
        ti.set_result "ack"
        return ti
      when ClearProtocolConst.NAK
        # puts "Test Harness-Message Handler-NACK received"
        #Transaction_Module.nack_transaction message.tx_number
        ti = TransactionItem.new message.tx_number, message
        ti.set_result = "nack"
      else
        error_message = "message not an Ack or a Nack - discarded ### information: " + message
        log :warn, error_message
        #puts error_message
        return TransactionItem.new message.tx_number, error_message
      end
    end
    # puts "*****************end of test msg handler**********************"
  end
end
