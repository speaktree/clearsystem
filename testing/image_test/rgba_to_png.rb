require 'chunky_png'

puts '/home/master/clear/clearsystem/public/slot1/*.rgba'

Dir['/home/master/clear/clearsystem/public/slot1/*.rgba'].sort.each do |rgba_file_name|
  puts "reading image #{rgba_file_name}"
  image = ChunkyPNG::Image.from_rgba_stream(720, 576, File.read(rgba_file_name))
  image.save(rgba_file_name[rgba_file_name.rindex('/') + 1, rgba_file_name.length] + '.png')
  puts "save to #{rgba_file_name[rgba_file_name.rindex('/') + 1, rgba_file_name.length] + '.png'}"
end
