#$: << '.'

require_relative 'constants_and_settings'
require_relative 'controller'
require_relative '../ir_api'
require_relative 'message_handler'
	


# Engine main startup file
#  - Initialises all the components and APIS and connects to any other components
# Will receive all aspects of the engine throuhg ARGS - 5 or so ARGS

class SyncAgent 
  def initialize
    @controller = Controller.new self
    @controller.start_sync_agent
  end
end


SyncAgent.new

loop do
  sleep(0.01)	
end

