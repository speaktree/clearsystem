require 'yaml'

# Defines all application constants and settings

# Holds all real time settings that can be accessed or changed
class Settings

  def initialize config_file
    if !File.exists? config_file
      # create config.yml file automatically if not found in folder
      object = { :identifier => "SyncAgent",
                 :server_port => 51515,
                 :debug => true,
                 :mode => :development
      }
      File.open(config_file, 'wb') { |f| f.write(YAML.dump(object)) }        
    end

    yaml = YAML::load(File.open(config_file))
   
    # Creating meta programming class accessors as defined in the yaml file
    yaml.each_pair do |key, val|
      self.class.instance_eval do
        define_method(key) {return val}
      end
    end
  end
end

module Constants

  # Info Constants

  VERSION = "1.0 Alpha"

  # Operational Constants

  SETTINGS = Settings.new 'config.yml'

  DEBUG = SETTINGS.debug

  DATABASE_SETTINGS = ""

end

