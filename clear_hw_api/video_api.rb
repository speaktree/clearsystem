require 'open3'
require 'socket'
require 'active_record'
require_relative '../../clearcore/corelibs/ruby/clear_logger'
require_relative '../clear_admin_portal/app/models/slot.rb'
require_relative '../protocols/ruby/clear_protocol'
require_relative '../clear_admin_portal/config/environment.rb'

include Clear_Protocol

class VideoAPI
  def initialize 
    @ffmpeg_pid = 0
    @slot_no = 0
  end

  def compare first, second 
    #Compare prestart and poststart arrays and return the difference, which should be the pid of the new ffmpeg process
    diff = second - first 
    @ffmpeg_pid = diff[0].to_i
  end

  def start_video options  #(input_codec="rawvideo", size="720x576", input_format="video4linux2", video_device="/dev/video0", output_codec="libx264", output_format="mpegts", bitrate=768, framerate=25, output_url="udp://239.1.1.1:1234")
    if options.length == 1 
      #use the slot number to get the required parameters from the database 
      slot = Slot.find_by_slot_number(options[:slot])
      input_codec = slot.input_codec
      size = slot.size
      input_format = slot.input_format
      video_device = slot.video_device
      output_codec = slot.output_codec
      output_format = slot.output_format
      bitrate = slot.bitrate
      framerate = slot.framerate
      output_url = slot.output_url + ":#{slot.slot_number + 2000}"
    else 
      # Start with passed parameters
      input_codec = options[:input_codec]
      size = options[:size]
      input_format = options[:input_format]
      video_device = options[:video_device]
      output_codec = options[:output_codec]
      output_format = options[:output_format]
      bitrate = options[:bitrate]
      framerate = options[:framerate]
      output_url = options[:output_url]
    end
      prestart_pids = `ps -ef | grep ffmpeg | grep -v grep |   awk -F ' ' '{print $2}'`
      prestart_array = prestart_pids.split(/\n/).reject(&:empty?)
      log 'clearcommon', "Prestart array is : #{prestart_array}"
     
      if output_codec != "libx264"
        fork do
          [$stdout, $stderr].each { |fh| fh.reopen File.open("/dev/null", "w") }
          exec "ffmpeg -vcodec #{input_codec} -video_size #{size} -f #{input_format} -i #{video_device} -an -vcodec #{output_codec}  -b:v #{bitrate}k -f #{output_format} -r #{framerate} udp://#{output_url}"
        end
      else
        #Start the ffmpeg process
        fork do
          [$stdout, $stderr].each { |fh| fh.reopen File.open("/dev/null", "w") }
          #Below is the original line for clear_view
          #exec "ffmpeg -vcodec #{input_codec} -video_size #{size} -f #{input_format} -i #{video_device} -an -vcodec #{output_codec} -profile main -tune zerolatency -x264opts intra-refresh=1:vbv-bufsize=32 -b:v #{bitrate}k -f #{output_format} -r #{framerate} udp://#{output_url}"
          exec "ffmpeg -vcodec #{input_codec} -f #{input_format} -i /dev/video#{video_device.split("video")[1].to_i + 4} -an -vcodec #{output_codec} -profile:v main -deinterlace -tune zerolatency -x264opts intra-refresh=1:vbv-bufsize=32 -b:v #{bitrate}k -f #{output_format} -r #{framerate} udp://#{output_url}"
        end
      end
      log 'clearcommon', "ffmpeg process started"
      # Get list of ffmpegs pids again
      poststart_pids = `ps -ef | grep -v grep |  grep ffmpeg | awk -F ' ' '{print $2}'` 
      poststart_array = poststart_pids.split(/\n/).reject(&:empty?)
      log 'clearcommon',  "post_start_array is : #{poststart_array}"
      # Keep delta task pid
      compare(prestart_array, poststart_array)
      log 'clearcommon', "The new pid for the the created process is #{@ffmpeg_pid}"
      if @ffmpeg_pid != 0 
        return true
      else
        return false
      end
  end

  def stop_video
    # Nicely quit ffmpeg
    exit_status = `kill -9 #{@ffmpeg_pid}; echo $?`
    #Otherwise terminate it 
    if exit_status == 1
      `kill -9 {#@ffmpeg_pid}`
       log 'clearcommon', :warn, "Could not kill ffmpeg gracefully, terminated process #{@ffmpeg_pid} with interrupt."
     else
       log 'clearcommon', "FFmpeg process #{@ffmpeg_pid} died gracefully."
    end
      return true
  end

end


if __FILE__ == $0

  # Put testing code for this file right in there with all scenario state data that is needed
  # eg Define parameters that have to be passed to the function in here
  # Just hard code a scenario and test that the functions actually behave as expected
  video_api = VideoAPI.new
  video_api.start_video
  sleep 60
  video_api.stop_video
  #  ffmpeg_pid = video_api.compare(prestart_array, poststart_array )
end 
