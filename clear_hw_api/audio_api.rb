require_relative '../clear_admin_portal/config/environment.rb'
require_relative '../protocols/ruby/clear_protocol'
require_relative '../../clearcore/corelibs/ruby/clear_logger'

include Clear_Protocol

class AudioAPI

  attr_accessor :ffmpeg_pid
  attr_accessor :slot_number
  attr_accessor :path
  attr_accessor :time_out
  
  @@AUDIO_PORT_DELTA = 3000
  @@THRESHOLD = 4

  def initialize slot_number, time_out 
    @ffmpeg_pid = 0
    @slot_number = slot_number
    @time_out = time_out
    @path = ''
    determine_path slot_number
  end 

  def test_for_volume
    get_audio_from_device
    determine_change_over_threshold
  end 

private

  def determine_change_over_threshold
    mean_volume = 0
    max_volume = 0
    File.foreach("#{@path}volume_output").each_with_index do |line, index|
      max_vol_index = line.index "max_volume"
      if max_vol_index != nil
        max_vol_line = line
        max_volume = max_vol_line[48..52].to_f
      end
    end 
    File.foreach("#{@path}volume_output").each_with_index do |line, index|
      mean_vol_index = line.index "mean_volume"
      if mean_vol_index != nil
        mean_vol_line = line
        mean_volume = mean_vol_line[48..52].to_f
       end
    end
    puts (max_volume - mean_volume) > @@THRESHOLD
    return (max_volume - mean_volume) > @@THRESHOLD
  end 

  def start_stream
    slot = Slot.find_by_slot_number @slot_number
    output_url = slot.output_url + ":#{slot.slot_number + @@AUDIO_PORT_DELTA}"
    audio_device = slot.audio_device
    prestart_pids = `ps -ef | grep ffmpeg | grep -v grep |   awk -F ' ' '{print $2}'`
    prestart_array = prestart_pids.split(/\n/).reject(&:empty?)
    fork do
      [$stdout, $stderr].each { |fh| fh.reopen File.open("/dev/null", "w") }
      exec "ffmpeg -acodec pcm_s16le -y -f alsa -ar 16000 -ac 1 -i hw:#{audio_device},0 -acodec libmp3lame -b:a 16k  -f mpegts udp://#{output_url}"
    end 
    poststart_pids = `ps -ef | grep -v grep |  grep ffmpeg | awk -F ' ' '{print $2}'` 
    poststart_array = poststart_pids.split(/\n/).reject(&:empty?)
    diff = poststart_array - prestart_array 
    @ffmpeg_pid = diff[0].to_i
    sleep 2
    puts "time_out is #{@time_out}"
    fork do 
      [$stdout, $stderr].each { |fh| fh.reopen File.open("#{@path}volume_output", "w") }
      exec "ffmpeg -t #{@time_out + 5} -f mpegts -i udp://@#{output_url} -af volumedetect -f null vol.txt"
    end 
    puts "done"
    sleep @time_out + 10
  end 

  def get_audio_from_device
    slot = Slot.find_by_slot_number @slot_number
    audio_device = slot.audio_device
    fork do
      [$stdout, $stderr].each { |fh| fh.reopen File.open("/dev/null", "w") }
      exec "ffmpeg -t #{@time_out} -acodec pcm_s16le -y -f alsa -ar 16000 -ac 1 -i hw:#{audio_device},0 -acodec libmp3lame -b:a 16k  -f mpeg #{@path}captured_audio.mpeg"
    end 
    sleep (@time_out + 3 )
    fork do 
      [$stdout, $stderr].each { |fh| fh.reopen File.open("#{@path}volume_output", "w") }
      exec "ffmpeg -f mpeg -i #{@path}captured_audio.mpeg -af volumedetect -f null vol.txt"
    end 
  end 

  def end_stream 
    exit_status = `kill #{@ffmpeg_pid}; echo $?`
    if exit_status == 1
      `kill -9 {#@ffmpeg_pid}`
       puts "Could not kill ffmpeg gracefully, terminated process #{@ffmpeg_pid} with interrupt."
     else
       puts  "FFmpeg process #{@ffmpeg_pid} died gracefully."
    end
      return true
  end 

  def determine_path slot_number
    working_dir = `pwd`.strip()
    common_folder = working_dir.index "clearsystem"
    @path = working_dir[0..common_folder]
    @path << "clearsystem"[1..-1]  
    @path << "/public/slot#{slot_number}/"
  end

end 

if __FILE__ == $0
  test_api = AudioAPI.new 1, 15
  test_api.test_for_volume
end
