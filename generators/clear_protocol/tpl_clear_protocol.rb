#!/usr/bin/env ruby

# WARNING! Generated code, DO NOT EDIT THIS FILE!
#
# Edit the template : <%= template_name %>
# Regenerate with   : <%= $0 %>
# This generated on : <%= Time.now.to_s %>
#
# Generic Clear Protocol Message decoder and encoder
# Johann Eloff, Feb 2013
#

require_relative '../../../clearcore/corelibs/ruby/convert_binary'
require_relative '../../../clearcore/corelibs/ruby/convert_general'

include Corelib

module Clear_Protocol

  class ClearProtocolConst
    # Define all the constants
    def self.HEADER_SIZE
      12
    end

    # Categories Types
    #TODO Get this from the Region parts??
    def self.COMMON_MESSAGES
      100
    end
    def self.ENGINE_MESSAGES
      200
    end
    def self.MASTER_MESSAGES
      300
    end
    def self.SLAVE_MESSAGES
      300
    end

    # Command Types
    <% ud.types.each do |t| %>
    def self.<%= t.name.underscore.upcase.ljust(35) %>
      <%= t.command_type %>
    end
    <% end %>

  end

  # Header Offsets
  class ClearProtocolHeaderOffsets
    def self.CommandCategory
      0
    end
    def self.CommandType
      2
    end
    def self.TimeStamp
      4
    end
    def self.TxNumber
      8
    end
  end

  # Simple class to hold raw binary Cleaar Protocol data. Provides integrity checks and can
  # decode the header. Further (message body) decoding requires conversion to a specific Clear Protocol object
  # using ToClearProtocolObject().
  class ClearProtocolBinary

    attr_reader :data

    def self.XML_ROOT
      "clear_command"
    end

    # Binary Array initialization
    def initialize(params)
      # Check the length of the params to decide what to do
      if params.length == 1
        # check that the type is correct and that the length is correct
        raise ArgumentError, "data needs t be an Array" unless params[:data].class == Array
        raise ArgumentError, "A valid binary Clear Protocol message needs at least #{ClearProtocolConst.HEADER_SIZE} bytes" unless params[:data].length >= ClearProtocolConst.HEADER_SIZE
        @data = params[:data]
      else
        if params.length == 4
          #TODO add type checking for the parameters here
          @data = Array.new(ClearProtocolConst.HEADER_SIZE)
          # Add the values into the header array
          ConvertBinary.ushort_short_insert_be(@data, ClearProtocolHeaderOffsets.CommandCategory, params[:cmd_cat])
          ConvertBinary.ushort_short_insert_be(@data, ClearProtocolHeaderOffsets.CommandType, params[:cmd_type])
          ConvertBinary.ulong_long_insert_be(@data, ClearProtocolHeaderOffsets.TimeStamp, ConvertGeneral.get_seconds_since_2000(params[:time_stamp]))
          ConvertBinary.ulong_long_insert_be(@data, ClearProtocolHeaderOffsets.TxNumber, params[:tx_number])
        else
          raise ArgumentError, "Invalid parameter list received"
        end
      end
    end

    def xml_header_fields
      ["cmd_cat", "cmd_type", "time_stamp", "tx_number"]
    end

    # Get accessor for header fields
    # The command category
    def cmd_cat
      ConvertBinary.to_ushort_short_be(@data[ClearProtocolHeaderOffsets.CommandCategory,2], 0)
    end
    # The command type
    def cmd_type
      ConvertBinary.to_ushort_short_be(@data[ClearProtocolHeaderOffsets.CommandType,2], 0)
    end
    # The timestamp of the message
    def time_stamp
      ConvertGeneral.get_time_from_seconds(ConvertBinary.to_ulong_long_be(@data[ClearProtocolHeaderOffsets.TimeStamp,4], 0))
    end
    # A default protocol formatted representation of the time
    def time_stamp_formatted
       time_stamp.strftime("%Y/%m/%d %H:%M:%S")
    end
    # Identifies the unique transaction number
    def tx_number
      ConvertBinary.to_ulong_long_be(@data[ClearProtocolHeaderOffsets.TxNumber,4], 0)
    end

    # This byte array represents the body of the binary ClearProtocol message originally used to construct
    # this object, excluding the header
    def body
      if @data.length > ClearProtocolConst.HEADER_SIZE - 1
        @data[ClearProtocolConst.HEADER_SIZE, @data.length - ClearProtocolConst.HEADER_SIZE]
      else
        return nil
      end
    end

    # This byte array represents the header of the binary Clear Protocol message originally used to construct
    # this object, or the header manually constructed manually via the alternate constructor
    def header
      if @data.length > ClearProtocolConst.HEADER_SIZE - 1
        @data[0, ClearProtocolConst.HEADER_SIZE]
      end
    end

    # Converts to a specific ClearProtocolObject based on the Command Category and Command Type of the
    # body. The resulting object can be tested to see where it is in the Clear Protocol hierarchy.
    # Example:
    # ClearProtocolBinary clearBinary = new ClearProtocolBinary(new byte[] { ... });
    # Object clearProtocolObject = clearBinary.ToClearProtocolObject();
    # clearProtocolObject.GetType() == typeof(TheObjectToTest));         //true
    def to_clear_protocol_object()
      case cmd_cat
        when ClearProtocolConst.COMMON_MESSAGES
        case cmd_type
        <% ud.types.each do |t| %>
          <% if t.region.include? "Common Messages" %>
            when ClearProtocolConst.<%= t.name.underscore.upcase %>
              return <%= t.name %>.new(:data => @data)
          <% end %>
        <% end %>
        end
        when ClearProtocolConst.ENGINE_MESSAGES
        case cmd_type
        <% ud.types.each do |t| %>
          <% if t.region.include? "Engine Messages" %>
            when ClearProtocolConst.<%= t.name.underscore.upcase %>
              return <%= t.name %>.new(:data => @data)
          <% end %>
        <% end %>
        end
      end
      return nil
    end
  end

  # Encapsulates the data so that it can be get and set through values
  # Ruby only has Number(All numbers) and Time and []
  class ClearProtocolDataObject

  # Typed constructed so that an empty body list can be build
  def initialize(params)
    if params.length == 1
      @type = params[:type]
      # Just initialise empty body objects depending on the type
      if @type == "T"
        @data = Array.new(4)
        set_value Time.utc(2000,1,1)
      end
      if @type == "l" || @type == "L"
        @data = Array.new(4)
        set_value 0
      end
      if @type == "b"
        @data = Array.new(1)
        set_value 0
      end
      if @type == "w" || @type == "W"
        @data = Array.new(2)
        set_value 0
        end
        if @type == "S"
          @data = [0]
        end
        if @type == "D"
            data = []
        end
      else
        # The binary representation of the data
        @data = params[:data]
        # How to format the data to and from binary
        @type = params[:type]
    end
  end

    # Gets the value representation from the data depending on the fromat
    def get_value
      # Switch the format_object types
      case @type
        when "T"
          return ConvertGeneral.get_time_from_seconds(ConvertBinary.to_ulong_long_be(@data, 0))
        when "b"
          return @data[0]
        when "w"
          return ConvertBinary.to_ushort_short_be(@data, 0)
        when "W"
          return ConvertBinary.to_ushort_short_be(@data, 0)
        when "l"
          return ConvertBinary.to_ulong_long_be(@data, 0)
        when "L"
          return ConvertBinary.to_ulong_long_be(@data, 0)
        when "S"
          # Means it is just a \0
          if @data.length == 1
            return ""
          else
            # Just get the value without the \0 c-string marker
            str = ""
            @data[0..@data.length - 2].each { |val| str << val.chr }
            return str
          end
        when "D"
          return @data
      end
    end

    # Sets the internal data representation from the value depending on the format
    def set_value(value)
      # Switch the format_object types
      case @type
        when "T"
          ConvertBinary.ulong_long_insert_be(@data, 0, ConvertGeneral.get_seconds_since_2000(value))
        when "b"
          # TODO: Might have to put a test here to see that it is smaller than a byte?????
          @data[0] = value
        when "w"
          ConvertBinary.ushort_short_insert_be(@data, 0, value)
        when "W"
          ConvertBinary.ushort_short_insert_be(@data, 0, value)
        when "l"
          ConvertBinary.ulong_long_insert_be(@data, 0, value)
        when "L"
          ConvertBinary.ulong_long_insert_be(@data, 0, value)
        when "S"
          #always reset the value for the string
          @data = []
          value.each_byte {|b| @data << b}
          # Just add the null terminating character if it is not there
          if @data[@data.length-1] != 0
            @data << 0
          end
        when "D"
          @data = value
      end
    end

    # Returns the internal data representation of the data
    def get
      return @data
    end

    # Create a new ClearProtocolDataObject based on the format specified
    # Extracts the data from the current body using the format object and returns
    # a new ClearProtocolDataObject
    # Also removes the data from the front of the buffer so that continuous parsing can be done
    def self.extract(working_data, type)
      raise ArgumentError, "working_data cannot be nil" unless working_data != nil
      # the data buffer
      data = []

      # Do all the 4 length things together
      if type == "T" || type == "l" || type == "L"
        # Always test if there is enough data in the buffer
        if working_data.length >= 4
          data = working_data[0..3]
          working_data.slice!(0, 4)
          return ClearProtocolDataObject.new(:data => data, :type => type)
        else
          return nil
        end
      end
      if type == "b"
        if working_data.length >= 1
          data << working_data[0]
          working_data.slice!(0, 1)
          return ClearProtocolDataObject.new(:data => data, :type => type)
        else
          return nil
        end
      end
      if type == "w" || type == "W"
        if working_data.length >= 2
          data = working_data[0..1]
          working_data.slice!(0, 2)
          return ClearProtocolDataObject.new(:data => data, :type => type)
        else
          working_data = []
          return nil
        end
      end
      if type == "S"
        # Will have to keep on popping them off until the null terminating character is found
        index = 0
        # make length start at 1 because event if it is just \0 you still want to chop it off
        length = 1
        while working_data[index] != 0
          data << working_data[index]
          index += 1
          length += 1
        end
        # Just add the terminating character
        data << 0
        # chop of the front of the buffer
        working_data.slice!(0, length)
        return ClearProtocolDataObject.new(:data => data, :type => type)
      end
      if type == "D"
        working_data.each do |val|
          data << val
        end
        working_data.slice!(0, working_data.lenght - 1)
        return ClearProtocolDataObject.new(:data => data, :type => type)
      end
    end
  end

# This class wraps an ClearProtocolBinary with functionality to interpret the message body, extracting
# all tags to a dictionary of Array containing instances of tag values. It serves as a base
# for each ClearProtocolObject Type.
class ClearProtocolObject < ClearProtocolBinary

  def self.XML_ROOT
    "command"
  end

  def initialize(body_format, params)
    super(params)
    raise ArgumentError, "ClearProtocolObject: Cannot instantiate without body_format, fix specific object constructor.If this Clear Protocol object has no body, pass '-' as the body_format." unless body_format.length > 0
    if body_format[body_format.length - 1] != 'D'         # no DATA(D) data type at end
      if body_format.include?('D')                      # yet there is a D in the body format
        raise ArgumentError, "Cannot construct body objects from " + body_format + ": data type 'D' must be at the end"
      end
    end
    if body_format == "-"
      @body_format = ""
    else
      @body_format = body_format
    end
    @body_objects = []
    if params.length == 1
      build_body_list true
    else
      build_body_list false
    end

    @ct_index = nil

  end

  # Returns a binary representation of the data
  def to_binary
    data = []
    @body_objects.each { |bo| data = data + bo.get }
    return header + data
  end

  # TODO Revisit this to_xml -> very gaga!!! Will remove when xml not used any more
  def to_xml indent
    # Put the root in
    xml_string = "<#{ClearProtocolBinary.XML_ROOT}>"
    xml_string +=  "\n\t" if indent
    # Add the header details
    self.xml_header_fields.each do |field|
      xml_string +=  "<#{field}>" + method(field).call.to_s + "</#{field}>"
      xml_string +=  "\n\t" if indent
    end
    xml_string += "<#{ClearProtocolObject.XML_ROOT}>"
    xml_string +=  "\n\t\t" if indent
    complex_type = false
    complex_closing = ""
    self.xml_fields.each_with_index do |field, index|
      if field == "-"
        complex_type = true
        next
      end

      if !complex_type
        xml_string +=  "<#{field}>" + method("get_" + field).call.to_s + "</#{field}>"
        xml_string +=  "\n\t\t" if indent
      else
        # Add the complex field stuff
        complex_closing = field
        index_in_body = get_index_in_body

        # Continue while there is data
        while index_in_body < body.length
          # Add the complex type field string
          xml_string += "<#{field}>"
          xml_string += "\n\t\t\t" if indent
          index_xml_names = @ct_index + 2
          @body_format[@ct_index..@body_format.length - 1].each_char do |char|
            # The value to se to to add to the xml
            xml_value = ""
            # Get the corresponding value out of the body depending on its type and increment the index
            case char
              when "T"
                xml_value = ConvertGeneral.get_time_from_seconds(ConvertBinary.to_ulong_long_be(body, index_in_body)).to_s
                index_in_body += 4
              when "b"
                xml_value = body[index_in_body].to_s
                index_in_body += 1
              when "w"|"w"
                xml_value = ConvertBinary.to_ushort_short_be(body, index_in_body).to_s
                index_in_body += 2
              when "l"|"L"
                ml_value = ConvertBinary.to_ulong_long_be(body, index_in_body).to_s
                index_in_body += 4
              when "S"
                while body[index_in_body] != 0
                  xml_value << body[index_in_body].chr
                  index_in_body += 1
                end
              end
              xml_string += "<#{xml_fields[index_xml_names]}>" + xml_value + "</#{xml_fields[index_xml_names]}>"
              xml_string += "\n\t\t\t" if indent
              index_xml_names += 1
          end
          # Just slice of the last tab
          xml_string = xml_string.slice(0..xml_string.length - 2)
          # Add the closing field
          xml_string += "</#{complex_closing}>"
          xml_string += "\n\t\t" if indent
        end
        # Break out of the loop after the complex type was parsed
        break
      end

    end
    # Just slice of the last tab
    xml_string = xml_string.slice(0..xml_string.length - 2)

    # Add the end of the record
    xml_string += "</#{ClearProtocolObject.XML_ROOT}>"
    xml_string += "\n" if indent
    # Add the end of the message
    xml_string += "</#{ClearProtocolBinary.XML_ROOT}>"
  end

  private

  def get_index_in_body
    # The index in the body
    index_in_body = 0
    index = 0
    @body_format.each_char do |char|
      if index < @ct_index
        case char
          when "T"
            index_in_body += 4
          when "b"
            index_in_body += 1
          when "w"
            index_in_body += 2
          when "W"
            index_in_body += 2
          when "l"
            index_in_body += 4
          when "L"
            index_in_body += 4
          when "S"
            index_in_body += @body_objects[index].get.length
          end
        end
        index += 1
      end
      return index_in_body
  end

  def build_body_list from_data
    raise ArgumentError, "Clear Protocol Message too short to be valid" unless self.body != nil
    work_body = Array.new(self.body)

    # iterate over the body_format type by type
    @body_format.each_char do |type|
      if from_data
        @body_objects << ClearProtocolDataObject.extract(work_body, type)
      else
        @body_objects << ClearProtocolDataObject.new(:type => type)
      end
    end
  end
end

    # Tag format strings specify a tag type for each tag number. Strings consist of:
    #
    # T: Timestamp: 4 bytes, unsigned int32 indicating seconds since 1 Jan 2000 in UTC
    #
    # b: Byte:      1 byte
    # w: Word:      2 bytes, signed
    # W: Word:      2 bytes, unsigned
    # l: Long:      4 bytes, signed
    # L: Long:      4 bytes, unsigned
    # S: String:    C-String null terminated
    # D: Data       Rest of frame
    # x: NoType:    0 bytes
    # n: nop:       This tag is not in the spec
    #
    # So, "Sbbb" means that tag 1 is a String, tags 2, 3, 4 are bytes.
    <% ud.types.each_with_index do |t, t_index| %>
    class <%= t.name %> < ClearProtocolObject
      def initialize(params)
        params_to_super = nil
        # constructed from data
        if params.has_key? :data
          params_to_super = params
        else
          # has a time_stamp and tx_number
          if params.has_key?(:time_stamp) && params.has_key?(:tx_number)
            params_to_super = {:cmd_cat => <%= t.category %>, :cmd_type => <%= t.command_type %>, :time_stamp => params[:time_stamp], :tx_number => params[:tx_number]}
          else
            # only has tx_number and will be created for time now
            params_to_super = {:cmd_cat => <%= t.category %>, :cmd_type => <%= t.command_type %>, :time_stamp => Time.now, :tx_number => params[:tx_number]}
          end
        end
        super("<%= t.to_format %>", params_to_super)
        <% t.tags.each_with_index do |tag, tag_index| %>
        <% if tag.type == "CT" %>
          @ct_index = <%= tag_index %>
        <% end %>
        <% end %>
      end

      def xml_fields
        <% xml_field_string = "" %>
        <% t.tags.each_with_index do |tag, tag_index| %>
          <% if tag.type == "CT" %>
            <% xml_field_string += "\"\-\",\"#{tag.to_xml_name}\"," %>
          <% else %>
            <% xml_field_string += "\"#{tag.to_xml_name}\"," %>
          <% end %>
        <% end %>
        [<%= xml_field_string.slice(0..xml_field_string.length - 2)%>]
      end

      #TODO add type checking per set for these values depending on type, otherwise a user could add anything!!!!!
      <% has_ct = false %>
      <% t.tags.each_with_index do |tag, tag_index| %>
      # <%= tag.desc %>
      <% if tag.type == "CT" %>
      <% has_ct = true %>
      def ct_<%= tag.to_xml_name %>_body
        ct_data_index = 0
        @body_objects.each_index do |index|
          if index < @ct_index
            ct_data_index += @body_objects[index].get.length
          end
        end
        ct_data_index
      end
      def ct_<%= tag.to_xml_name %>_data
        ct_data_index = header.length
        @body_objects.each_index do |index|
          if index < @ct_index
            ct_data_index += @body_objects[index].get.length
          end
        end
        ct_data_index
      end
      <% else %>
      def get_<%= tag.to_xml_name %>
        <% if has_ct %>
        return @body_objects[<%= tag_index - 1 %>].get_value
        <% else %>
          return @body_objects[<%= tag_index %>].get_value
        <% end %>
      end
      def set_<%= tag.to_xml_name %>(value)
        <% if has_ct %>
          @body_objects[<%= tag_index - 1 %>].set_value(value)
        <% else %>
          @body_objects[<%= tag_index %>].set_value(value)
        <% end %>
      end
    <% end %>
    <% end %>
    end
  <% end %>

end





