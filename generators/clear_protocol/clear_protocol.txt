# Clear Command Specification
# ========================
#
# This is a Big Endian Potocol. This is a transaction based protocol. So all requests will receive a response sharing the same 
# transaction number. This protocol has sections or categories and certian applications in the stack will only ever respond 
# to their specific commands or if a message is not recognised an approporiate NAK will be sent. All modules in the stack will 
# implement the Common messages as well as their specific operational messages.
#
# About this document
# ===================
#
# This document is read by programs to automatically generate code from the specification. The BTD message type and tag sections
# are in a specific format, which you must know in order to edit these sections. Tabs may be used, but they MUST be set to 8
# characters wide. These are expanded to 8 character wide spaces during parsing. For ease of use in text editors, 
# 
# please keep the width of this document to 130 tab-expanded columns and under:
# |-------------------------------------------------------------------------------------------------------------------------------|
#
# Flag (bit) mappings are not parsed, they only specified in comments, like so:
#
# StatusFlags		U16		Flags indicating current unit status (not alarm reason):
#					Bit 0 - GPS Module Comms Fail
#                                       Bit 1 - GPS Lock Fail
#                                       Bit 2 - GPS Antenna Short
#                                       Bit 3 - GPS Antenna Open
# 1 Definitions
# =============
#
# 1.1 Data Type Definitions
# =========================
#
# The BTD message format is in "raw" data form, i.e. message field values per byte may be from 0x00 to 0xFF inclusive, for
# all bytes in the message. This does not mean, of course, that some of the fields may, per definition, be only ASCII
# characters.
#
# Throughout the document the following data types will be used:
#
# Type            Size (Bytes)    Description
# -----------------------------------------------------------
# CT		  0		  Is an indicator for a complex type, anything inside there can be repeated over and over
# BIT             1               Boolean (Zero = FALSE, Non-zero = TRUE)
# U8              1               Unsigned 8-bit integer
# U16             2               Unsigned 16-bit integer
# U32             4               Unsigned 32-bit integer
# S8              1               Signed 8-bit integer
# S16             2               Signed 16-bit integer
# S32             4               Signed 32-bit integer
# STRING          varaible        Strings are encoded as a series of ASCII
#                                 characters with a C string format, null termintated
# DATA            variable        Data is exactly like a string, except that since it encodes 8-bit
#                                 binary data, it can contain NULL characters. This data type
#                                 has to consume the whole message body since there is no terminator
#                                 character, so messages using this type can only have one such field at the end of the message.
# TIMESTAMP       4               Time is coded as an U32 representing the
#                                 number of seconds since 1 January 2000 in
#                                 universal time coordinates (UTC). 
#
# Time is coded as an U32 representing the number of seconds since 1 January 2000 in universal time coordinates (UTC).
# This will give a time-span of approximately 136 years, i.e., until the year 2136.
#
#
#
# 2 Clear Protocol Frame Format
# ================================================
#
# The Clear Protocol will have the following format:
#
# Name			Data Type	XML Name	Description
# -----------------------------------------------------------
# 
# CommandCategory	U16		cmd_cat		The command category
# CommandType		U16		cmd_type	The command type
# TimeStamp		TIMESTAMP	time_stamp	The timestamp of the message
# TxNumber		S32		tx_number	Identifies the unique transaction number
#                                				
# [Body of message]			Contains a specific type of body type
#
# CRC             	U16             The CRC field supports error checking of messages. It contains the accumulated U16 (modulo
#                         	        65536) sum of all the preceding bytes in the message, from the Message Length field
#                               	inclusive.
#
# Header Offsets: CommandCategory = 0, CommandType = 2, TimeStamp = 4, TxNumber = 8
#
# 3 Specific Message Formats
# ==========================
#
# ================================================================================================================================
[R] Clear Protocol Common Messages 100
# =================================================================================================================================

[T] 100 0 Ack
Name			Data Type	XML Name		Description

[T] 100 255 Nak 
Name			Data Type	XML Name		Description
ErrorCode		U8		error_code		Number assigned as an error code:
                                                                  1. Could not stop video stream
                                                                  2. Invalid video command 
                                                                  3. Could not start video stream
                                                                  4. Could not take the screenshot 
                                                                  5. Could net get zaptime 
                                                                  6. Could not analyse compare image 
                                                                  7. Could not detect audio 
                                                                  8. 
                                                                  9. 
                                                                 10. No IR Netbox configured
                                                                 11. Invalid Slot
                                                                 12. Could not send IR command
                                                                  
Reason			STRING		reason			A string representing the reason for the Nak

[T] 100 1 Notification
Name			Data Type	XML Name		Description
NotifyCode		U8		notify_code		A number assigned as a notification code:
                                                                  1. Requested Client on Slot:x streaming status
                                                                  2. Client on Slot:x not streaming
                                                                  3. Requested Client on Slot:x stream cache status
                                                                  4. Client 0n Slot:x does have requested stream cached
                                                                  5. Client 0n Slot:x does not have requested stream cached
                                                                  6. Requested Client on Slot:x to cache stream
                                                                  7. Requested Client on Slot:x to start streaming
                                                                  8. Client on Slot:x streaming 
                                                                  9. Client on Slot:x caching stream progress equals:y%
Reason			STRING		reason			A string representing the reason for the Notification   


# ================================================================================================================================
[R] Clear Protocol Engine Messages 200
# =================================================================================================================================
[T] 200 1 PushButton
Name			Data Type	XML Name		Description
SlotNumber		U16		slot_number		The slot on which you want to push the button
ButtonCommand		U8		button_command		Commands:
                                                                1.  Power
                                                                2.  VolumePlus
                                                                3.  VolumeMinus
                                                                4.  ChannelPlus
                                                                5.  ChannelMinus
                                                                7.  Mute
                                                                8.  Backup
                                                                9.  TvGuide
                                                                10. Info
                                                                11. Menu
                                                                12. GoTv
                                                                13. Rew
                                                                14. Play
                                                                15. Stop
                                                                16. Pause
                                                                17. Record
                                                                18. Ffwd
                                                                19. Ok
                                                                20. OnDemand
                                                                21. Alt
                                                                22. BoxOffice
                                                                23. Option
                                                                24. Help
                                                                25. PlayList
                                                                26. Search
                                                                27. Arc
                                                                28. Exit
                                                                29. Tv
                                                                30. Profile
                                                                31. Left
                                                                32. Right
                                                                33. Up
                                                                34. Down
                                                                35. 0
                                                                36. 1
                                                                37. 2
                                                                38. 3
                                                                39. 4
                                                                40. 5
                                                                41. 6
                                                                42. 7
                                                                43. 8
                                                                44. 9
                                                                45. Language
                                                                46. Exit
                                                                47. Red
                                                                48. Blue
                                                                49. White
                                                                50. Yellow
                                                                51. Green
                                                                52. Help
                                                                53. Subtitles
                                                                54. Audio




[T] 200 2 StartVideo
Name			Data Type	XML Name		Description

[T] 200 3 StartVideoWithParamaters
Name			Data Type	XML Name		Description

[T] 200 4 StopVideo
Name			Data Type	XML Name		Description

[T] 200 5 StartEngine
Name			Data Type	XML Name		Description
SlotNumber              U16             slot_number             Slot number of engine to be started

[T] 200 6 StopEngine
Name			Data Type	XML Name		Description
SlotNumber              U16             slot_number             Slot number of engine to be stopped

[T] 200 7 PowerOnSlot
Name	         	Data Type	XML Name		Description
SlotNumber              U16             slot_number             Slot number of engine to start logging 

[T] 200 8 PowerOffSlot
Name	         	Data Type	XML Name		Description
SlotNumber              U16             slot_number             Slot number of engine to start logging 

[T] 200 9 PowerCycleSlot
Name	         	Data Type	XML Name		Description
SlotNumber              U16             slot_number             Slot number of engine to start logging 

[T] 200 10 CaptureImage
Name	          	Data Type	XML Name		Description
SlotNumber              U16             slot_number             Slot number of engine to start logging 
Type                    STRING          type                    Type of picture
Rectangles              STRING          rectangles              Hash of rectangles to be evaluated

[T] 200 11 OCR
Name	          	Data Type	XML Name		Description
SlotNumber              U16             slot_number             Slot number of engine to start logging 
Rectangles              STRING          rectangles              Hash of rectangles to be evaluated
SearchString            STRING          search_string           The string that you're looking for 
CheckSpelling           U8              check_spelling          Must check the spelling of the OCR string found

[T] 200 12 DetectSound
Name	          	Data Type	XML Name		Description
SlotNumber              U16             slot_number             Slot number of engine to start logging 
TimeOut                 U16             time_out                Time difference between images

[T] 200 13 DetectMovement
Name	          	Data Type	XML Name		Description
SlotNumber              U16             slot_number             Slot number of engine to start logging 
Percent                 U8              percent                 Percentage change in the image
TimeOut                 U16             time_out                Time difference between images
Save                    STRING          save                    Boolean for whether to save or not
Rectangles              STRING          rectangles              Hash of rectangles to be evaluated

[T] 200 14 ComparePicture
Name	          	Data Type	XML Name		Description
SlotNumber              U16             slot_number             Slot number of engine to start logging 
ImagePath               STRING          image_path              Path to compare image
Percent                 U8              percent                 Percentage change in the image
TimeOut                 U16             time_out                Time difference between images
Rectangles              STRING          rectangles              Rectangles to be evaluated
ImageRectangels         STRING          image_rectangles        Image paths rectangles

[T] 200 15 StartSerialLogging
Name			Data Type	XML Name		Description
SlotNumber              U16             slot_number             Slot number of engine to start logging 
ClientIp                STRING          client_ip               IP address of client

[T] 200 16 SerialLoggingRequestResponse
Name			Data Type	XML Name		Description
PortNumber              U16             port_number             TCP connection port streaming serial logging data

[T] 200 17 TestResponse
Name			Data Type	XML Name		Description
Result                  STRING          result                  Boolean result of the test
Data                    STRING          data                    Data returned

[T] 200 18 InitializeSlot
Name			Data Type	XML Name		Description
SlotNumber              U8              slot_number             Slot number to initialize

[T] 200 19 ZapTime
Name	          	Data Type	XML Name		Description
Rectangles              STRING          rectangles              Hash of rectangles to be evaluated
ButtonCommand           U8              button_command          Integer value of button_command 
ButtonCommandCount      U16             button_command_count    Number of times to press the above button_command
TimeOut                 U16             time_out                Time out waiting for blank screen to become picture

[T] 200 20 AnalyseCompareImage
Name	          	Data Type	XML Name		Description
Rectangles              STRING          rectangles              Hash of rectangles to be evaluated
ButtonCommand           U8              button_command          Integer value of button_command 
ImagePath               STRING          image_path              Path to compare image
Percent                 U8              percent                 Percentage change in the image
TimeOut                 U16             time_out                Time out waiting for blank screen to become picture

[T] 200 21 CheckColour
Name	          	Data Type	XML Name		Description
SlotNumber              U16             slot_number             Slot number of engine to start logging 
Percent                 U8              percent                 Percentage change in the image
Threshold               U8              threshold               Threshold of colour difference per pixel
Rectangles              STRING          rectangles              Rectangles to be evaluated
Red                     U8              red                     Red part of colour
Green                   U8              green                   Green part of colour
Blue                    U8              blue                    Blue part of colour

[T] 200 22 CatalogueValidation
Name	          	Data Type	XML Name		Description
Rectangles              STRING          rectangles              Rectangles to be evaluated
Url                     STRING          url                     REST Url
Type                    STRING          type                    Catalogue type

[T] 200 23 TakePicture
Name	          	Data Type	XML Name		Description
SlotNumber              U8              slot_number             slot number
Type                    STRING          type                    type of picture


# ================================================================================================================================
[R] Clear Protocol Master Messages 300
# =================================================================================================================================
								
# ================================================================================================================================
[R] Clear Protocol Slave Messages 400
# =================================================================================================================================
						

# vim:ts=8
