#!/usr/bin/env ruby
$LOAD_PATH << '.'
require 'protocol_api'
require 'erb'
require 'fileutils'

include FileUtils::Verbose

class Template
    def initialize(protocol_doc, template_name, target_name)
        @protocol_doc = protocol_doc
        @template_name = template_name
        @target_name = target_name 
    end

    def interpret
        ud = ProtocolDoc.new(@protocol_doc)

        template = File.new(@template_name).read
        template_name = @template_name

        #actual interpretation step
        erb = ERB.new(template, nil)

        file = File.open(@target_name, "w")
        file.write(erb.result(binding))
        file.close
    end
end

if ARGV.length != 3
    puts "Builds a target file from an btd spec document and template"
    puts
    puts "Usage: build <protocoldoc> <template> <target>" 
    puts
    exit(1)
end

protocoldoc, template, target = ARGV
puts "Building #{target} from #{template} using protocol specification #{protocoldoc}"
Template.new(protocoldoc, template, target).interpret


