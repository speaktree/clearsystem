require_relative '../../clearcore/corelibs/ruby/tcp_client'
require_relative 'message_handler'
require_relative 'constants_and_settings'
require_relative '../protocols/clear_view_protocol'
require 'socket'
require 'active_record'
require_relative '../clear_admin_portal/config/environment.rb'

include Clear_View_Protocol
include Clear_Protocol
include Constants 

class Controller

  def initialize slave
    @slave = slave
    @message_handler = MessageHandler.new self
  end

  def start_client node_id
    begin
      master_node = NodeType.find_by_node_type('master')
      @master_ip = master_node.node.ip
      @master_port = SETTINGS.master_port
      @tcp_client = TCPSocketClientSocket.new 
      @tcp_client.connect @master_ip , @master_port , method(:data_method), method(:notification_method)
      @message_handler.report_status []
      return true
    rescue=>e
      puts "Got Exception: "
      p e
      puts e.backtrace.join("\n")
      return false
    end
  end

  def send data
    puts "Send data to master method started"
    @tcp_client.send data
  end

  def data_method data
    puts "data_method -TCP CLIENT GOT DATA!!!"
    p data
      message_data = []
      data.each_byte do |b|
        message_data << b
      end
      p message_data
      clear_binary = ClearProtocolBinary.new(:data=>message_data)
      if clear_binary.cmd_cat == ClearProtocolConst.COMMON_MESSAGES || clear_binary.cmd_cat == ClearProtocolConst.ENGINE_MESSAGES
        @message_handler.handle_message @master_ip, clear_binary.to_clear_protocol_object
      else
        @message_handler.handle_message @master_ip, ClearViewProtocolBinary.new(:data=>message_data).to_clear_view_protocol_object
      end
  end

  def notification_method source, state, message
    puts 'Controller notification_method called!'
    p source
    p state
    p message
  end

 
end
